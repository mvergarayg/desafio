//
//  SyncManager.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import SwiftyJSON

class SyncManager {
    
    // MARK: - Singleton
    static let shared = SyncManager()
    
    func syncMenu(completion: @escaping (RestResponse<[FMovie]>) -> Void) {
        
        MovieManager.shared.getAll { (catRpta) in
            switch catRpta {
            case .failure(let error):
                completion(.failure(error))
                return
                
            case .success(let categorias):
                
                completion(.success(categorias))
            }
        }
        
    }
}
