//
//  MovieManage.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import SwiftyJSON

class MovieManager {
    
    static let shared = MovieManager()
    
    func getAll(completion: @escaping (RestResponse<[FMovie]>) -> Void) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        try? context.save()
        
        let categorias = FMovie.findAll(inContext: context)
        completion(.success(categorias))
    }
    
    func saveMovie(movie: FMovie, completion: @escaping(RestResponse<FMovie>) -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
               let context = appDelegate.persistentContainer.viewContext
               
               try? context.save()
               
        let movie = FMovie.findOrCreate(byId: movie.id!, inContext: context)
        movie.copy(from: ["title", movie.title])
        completion(.success(movie))
    }
    
}
