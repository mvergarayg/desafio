//
//  Constants.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import Foundation

enum Constants {
  
    enum Network {
        
        fileprivate static let urlInfoKey = "APP-URL"
        static var baseURL:String {
            return "https://api.themoviedb.org/3/"
        }
        
        static var trackTvUrl:String {
            return "https://api.trakt.tv/"
        }
    }
    
    enum Session {
        static let apiKey = "d610d8df0aa4af97a150e8d39c646841"
        static let userKey = "USER_SESSION"
        static let tokenKey = "USER_TOKEN"
        
    }
    
    enum TrakTv {
        static let clientId = "78c25409506a29f1752d244cf043e4209bbfd5db4f14d852c23737d7e8721c87"
        static let clientSecret = "6b9f1b42d54214d088474ee7767f6096f704b959014036a38d68993ed6c87db5"
        static let redirectURI = "traktkit://auth/trakt"
    }
}
