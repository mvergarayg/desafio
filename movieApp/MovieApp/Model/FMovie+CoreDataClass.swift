//
//  FavMovie+CoreDataClass.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON
import CloudKit

@objc(FMovie)
public class FMovie: NSManagedObject {

    func copy(from json:JSON) {
        id = json["id"].stringValue
        title = json["title"].stringValue
        release_date = json["release_date"].stringValue
        overview = json["overview"].stringValue
        vote_count = json["vote_count"].stringValue
    }
    
    var record:CKRecord {
        var aRecord:CKRecord
        if let cloudIdenfier = self.id {
            let recordId = CKRecord.ID(recordName: cloudIdenfier)
            aRecord = CKRecord(recordType: "Movie", recordID: recordId)
        } else {
            aRecord = CKRecord(recordType: "Movie") // INSERT
        }
        aRecord["id"] = id
        aRecord["title"] = title
        aRecord["release_date"] = release_date
        aRecord["overview"] = overview
        aRecord["vote_count"] = vote_count
        
        return aRecord
    }
    
    class func findOrCreate(byId id:String,
                            inContext context:NSManagedObjectContext) -> FMovie {
        return context.findOrCreate(byId: id)
    }
    
    class func findAll(inContext context:NSManagedObjectContext) -> [FMovie] {
        let nameSort = NSSortDescriptor(key: "title", ascending: true)
        return context.findAll(orderbBy: [nameSort])
    }
    
}

