//
//  NSManagedObjectContext+Query.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    
    func findOrCreate<T:NSManagedObject>(byId id:String) -> T {
        let entityName = String(describing: T.self)
        let request = NSFetchRequest<T>(entityName: entityName)
        request.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let result = try fetch(request)
            if let element = result.first {
               return element
            }
            
            let newElement = T(context: self)
            newElement.setValue(id, forKey: "id")
            return newElement
            
        } catch(let ex) {
            print(ex)
            let newElement = T(context: self)
            newElement.setValue(id, forKey: "id")
            return newElement
        }
    }
    
    func findAll<T:NSManagedObject>(orderbBy order:[NSSortDescriptor]) -> [T] {
        let entityName = String(describing: T.self)
        let request = NSFetchRequest<T>(entityName: entityName) // SELECT * FROM T
        request.sortDescriptors = order // order by
        
        let result = try? fetch(request)
        return result ?? []
    }
    
    func deleteAll () {
        
    }
}

