//
//  FavMovie+CoreDataProperties.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import Foundation
import CoreData

extension FMovie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FMovie> {
        return NSFetchRequest<FMovie>(entityName: "FMovie")
    }

    @NSManaged public var id: String?
    @NSManaged public var title: String?
    @NSManaged public var release_date: String?
    @NSManaged public var overview: String?
    @NSManaged public var vote_count: String?
}

// MARK: Generated accessors for meals
extension FMovie {
}

