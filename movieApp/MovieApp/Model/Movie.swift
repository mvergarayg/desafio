//
//  Movie.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import SwiftyJSON

class Movie:NSObject, NSCoding, Codable {
    
    var fbId: Int = 0;
    var id: String = "";
    var title: String = "";
    var release_date: String = "";
    var overview: String = "";
    var vote_count: String = "";
    
    override init() {
        super.init()
    }
    
    // MARK: - NSCoding
    func encode(with coder: NSCoder) { // from user to data
        coder.encode(id, forKey: "id")
        coder.encode(title, forKey: "title")
        coder.encode(release_date, forKey: "release_date")
        coder.encode(overview, forKey: "overview")
        coder.encode(vote_count, forKey: "vote_count")
    }
    
    required init?(coder: NSCoder) { // from data to user
        id = coder.decodeObject(forKey: "id") as? String ?? ""
        title = coder.decodeObject(forKey: "title") as? String ?? ""
        release_date = coder.decodeObject(forKey: "release_date") as? String ?? ""
        overview = coder.decodeObject(forKey: "overview") as? String ?? ""
        vote_count = coder.decodeObject(forKey: "vote_count") as? String ?? ""
    }
}


