//
//  DetailViewController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit
import Firebase

class DetailViewController: UIViewController {
    
    // Constants for state restoration.
    private static let restoreProduct = "restoreProductKey"
    
    // MARK: - Properties

    var movie: Movie!
    
    @IBOutlet private weak var yearLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton!
    
    @IBAction func saveToFavorites () {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        try? context.save()
        

        let database = Database.database()
        let ref = database.reference()
        let moviesRef = ref.child("movies")
        let currentMovieRef = moviesRef.child(movie.id)
        
        let values:[String:Any] = [
            "title":movie.title,
            "id": movie.id,
            "overview": movie.overview,
            "release_date": movie.release_date,
            "vote_count": movie.vote_count
        ]

        currentMovieRef.setValue(values)
        
        let  vc =  (self.navigationController?.viewControllers[0])! as! FavoriteTableViewController

        vc.searchController.searchBar.searchTextField.text = nil
        vc.searchController.searchBar.endEditing(true)
        navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Initialization
    
    class func detailViewControllerForProduct(_ movie: Movie) -> UIViewController {
        let storyboard = UIStoryboard(name: "NavigationView", bundle: nil)

        let viewController =
            storyboard.instantiateViewController(withIdentifier: "DetailViewController")
        
        if let detailViewController = viewController as? DetailViewController {
            detailViewController.movie = movie
        }
        
        return viewController
    }
    
    // MARK: - View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if movie.fbId > 0 {
            actionButton.setTitle("", for:.normal)
        } else {
            actionButton.setTitle("Save to Favorites", for:.normal)
        }
        
        
        title = movie.title
        
        titleLabel.text = "\(movie.title)"
        yearLabel.text = "\(movie.release_date)"
        overviewLabel.text = "\(movie.overview)"
    }
}

// MARK: - UIStateRestoration

extension DetailViewController {
    
    override func encodeRestorableState(with coder: NSCoder) {
        super.encodeRestorableState(with: coder)
        
        // Encode the movie.
        coder.encode(movie, forKey: DetailViewController.restoreProduct)
    }
    
    override func decodeRestorableState(with coder: NSCoder) {
        super.decodeRestorableState(with: coder)
        
        // Restore the product.
        if let decodedProduct = coder.decodeObject(forKey: DetailViewController.restoreProduct) as? Movie {
            movie = decodedProduct
        } else {
            fatalError("A product did not exist. In your app, handle this gracefully.")
        }
    }
    
}

