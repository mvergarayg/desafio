//
//  NavigationTMDBViewController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit

class NavigationTMDBViewController: UINavigationController{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "NavigationView", bundle: Bundle.main)
        let navCtl = storyboard.instantiateViewController(withIdentifier: "NavigationTMDBView") as! NavigationTMDBViewController
        // This view controller is interested in table view row selections.
        navigationController?.delegate = navCtl.delegate
    }
}
