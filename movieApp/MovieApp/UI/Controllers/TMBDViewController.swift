//
//  TMBDViewController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class TMDBViewController: UITableViewController {
    let tableViewCellIdentifier = "MovieCell"
    let loadingCellIdentifier = "LoadingCell"
    var tempContext:NSManagedObjectContext!
    
    // MARK: - Properties
    
    /// Data model for the table view.
    var movies:[Movie] = []
    var fetchingMore = false

    override func viewDidLoad() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        tempContext = appDelegate.persistentContainer.newBackgroundContext()
        
        let tableViewCellNib = UINib(nibName: tableViewCellIdentifier, bundle: nil)
        tableView.register(tableViewCellNib, forCellReuseIdentifier: tableViewCellIdentifier)
        
        let loadingNib = UINib(nibName: loadingCellIdentifier, bundle: nil)
        tableView.register(loadingNib, forCellReuseIdentifier: loadingCellIdentifier)
        
        // Make the search bar always visible.
        navigationItem.hidesSearchBarWhenScrolling = false
        
        getTopRatedMovies { (response) in
            switch response {
            case .failure(let error):
                self.showAlert(withTitle: "Error", message: error.localizedDescription)
                
            case .success(let movies):
                self.movies = movies
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - UITableViewDelegate

extension TMDBViewController {
    
    func movie(forIndexPath: IndexPath) -> Movie {
        return self.movies[forIndexPath.row]
    }
    
    func getTopRatedMovies(completion: @escaping (RestResponse<[Movie]>) -> Void) {
        fetchingMore = true
        
        let count: Int
        let service = MovieRepository()
        
        if self.movies.count == 0 {
            count = 1;
        } else {
            count = ((self.movies.count + 1) / 20) + 1
        }
        
        
        print("Getting Top rated movies. Page:\(count)")
        
        service.getAll(page: count) { (response) in
            switch response {
            case .failure(let error):
                completion(.failure(error))
                
            case .success(let jsons):
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    for json in jsons {
                        let movie: Movie = Movie()
                        movie.id = json["id"].stringValue
                        movie.title = json["title"].stringValue
                        movie.vote_count = json["vote_count"].stringValue
                        movie.release_date = json["release_date"].stringValue
                        movie.overview = json["overview"].stringValue
                        self.movies.append(movie)
                    }
                    self.tableView.reloadData()
                    self.fetchingMore = false
                })
                
                completion(.success(self.movies))
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension TMDBViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if section == 0 {
           return movies.count
       } else if section == 1 && fetchingMore {
           return 1
       }
       return 0
   }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if ((offsetY+scrollView.frame.height) > contentHeight) && !fetchingMore {
            getTopRatedMovies { (response) in
                switch response {
                case .failure(let error):
                    self.showAlert(withTitle: "Error", message: error.localizedDescription)
                    
                case .success(let movies):
                    print("More movies loaded after scroll. Number of movies \(movies.count)");
                }
            }
            
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath) as! MovieCell
            
            cell.titleLabel?.text = movies[indexPath.row].title
            cell.rateLabel?.text = movies[indexPath.row].vote_count
            cell.dateLabel?.text = movies[indexPath.row].release_date
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath) as! LoadingCell
            cell.spinner.startAnimating()
            return cell
        }
    }
}
