//
//  FavoriteTableViewController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class FavoriteTableViewController: UITableViewController {
    
    let tableViewCellIdentifier = "MovieCell"
    let loadingCellIdentifier = "LoadingCell"
    var tempContext:NSManagedObjectContext!
    
    // MARK: - Properties
    
    /// Data model for the table view.
    var movies = [Movie]()
    var fetchingMore = false

    /// Search controller to help us with filtering items in the table view.
    var searchController: UISearchController!
    
    /// Search results table view.
    private var resultsTableController: ResultsTableController!
    
    /// Restoration state for UISearchController
    var restoredState = SearchControllerRestorableState()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        tempContext = appDelegate.persistentContainer.newBackgroundContext()
        
        let nib = UINib(nibName: tableViewCellIdentifier, bundle: nil)
       tableView.register(nib, forCellReuseIdentifier: tableViewCellIdentifier)
       
       let storyboard = UIStoryboard(name: "ResultsTable", bundle: Bundle.main)
       resultsTableController = storyboard.instantiateViewController(withIdentifier: "ResultsTableController") as! ResultsTableController

       // This view controller is interested in table view row selections.
       resultsTableController.tableView.delegate = self
       
       searchController = UISearchController(searchResultsController: resultsTableController)
       searchController.delegate = self
       searchController.searchResultsUpdater = self as! UISearchResultsUpdating
       searchController.searchBar.autocapitalizationType = .none
       searchController.dimsBackgroundDuringPresentation = false
       searchController.searchBar.delegate = self // Monitor when the search button is tapped.

       // Place the search bar in the navigation bar.
       navigationItem.searchController = searchController
       
       // Make the search bar always visible.
       navigationItem.hidesSearchBarWhenScrolling = false
               
        let database = Database.database()
        let ref = database.reference()
        let moviesRef = ref.child("movies")
        
        moviesRef.observe(.value) { (snapshot) in
            let dict = snapshot.value as? [String:[String:Any]] ?? [:]
            
            var movies:[Movie] = []
            for (key, value) in dict {
                let aMovie = Movie()
                
                aMovie.fbId = key.count ?? 0
                aMovie.id = value["id"] as! String
                aMovie.title = value["title"] as! String
                aMovie.vote_count = value["vote_count"] as? String ?? ""
                aMovie.overview = value["overview"] as? String ?? ""
                aMovie.release_date = value["release_date"] as? String ?? ""
                movies.append(aMovie)
            }
            self.movies = movies
            self.tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Restore the searchController's active state.
        if restoredState.wasActive {
            searchController.isActive = restoredState.wasActive
            restoredState.wasActive = false
            
            if restoredState.wasFirstResponder {
                searchController.searchBar.becomeFirstResponder()
                restoredState.wasFirstResponder = false
            }
        }
    }

}

// MARK: - UITableViewDelegate

extension FavoriteTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let findMovie:Movie!
        let resultsController = searchController.searchResultsController as? ResultsTableController
        if !searchController.searchBar.text!.isEmpty {
            findMovie = resultsController?.filteredMovies[indexPath.row]
        } else {
            findMovie = movies[indexPath.row]
            
        }
         
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext

        try? context.save()
        // Set up the detail view controller to push.
        let detailViewController = DetailViewController.detailViewControllerForProduct(findMovie)
        navigationController?.pushViewController(detailViewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}

// MARK: - UISearchBarDelegate

extension FavoriteTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        updateSearchResults(for: searchController)
    }
    
}

// MARK: - UISearchControllerDelegate

// Use these delegate functions for additional control over the search controller.

extension FavoriteTableViewController: UISearchControllerDelegate {
    
    func presentSearchController(_ searchController: UISearchController) {
        //Swift.debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
        //Swift.debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        //Swift.debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        //Swift.debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        //Swift.debugPrint("UISearchControllerDelegate invoked method: \(#function).")
    }
    
}

// MARK: - UITableViewDataSource

extension FavoriteTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if section == 0 {
           return movies.count
       } else if section == 1 && fetchingMore {
           return 1
       }
       return 0
   }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath) as! MovieCell
            let cellMovie = movie(forIndexPath: indexPath)
            cell.titleLabel?.text = cellMovie.title
            cell.rateLabel?.text = cellMovie.vote_count
            cell.dateLabel?.text = cellMovie.release_date
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath) as! LoadingCell
            cell.spinner.startAnimating()
            return cell
        }
        
    }

    
}
