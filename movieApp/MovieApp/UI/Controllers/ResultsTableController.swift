//
//  ResultsTableController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//
import UIKit
import TraktKit

class ResultsTableController: UITableViewController {
    
    let tableViewCellIdentifier = "MovieCell"
    let loadingCellIdentifier = "LoadingCell"
    var filteredMovies = [Movie]()
    var fetchingMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: tableViewCellIdentifier, bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: tableViewCellIdentifier)
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredMovies.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath) as! MovieCell
            
            cell.titleLabel?.text = filteredMovies[indexPath.row].title
            cell.rateLabel?.text = filteredMovies[indexPath.row].release_date
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath) as! LoadingCell
            cell.spinner.startAnimating()
            return cell
        }
    }
}
