//
//  TabbedViewController.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit
//import Alamofire

class TabbedViewController: UITabBarController  {

    var tableView:UITableView!
    var movies:[Movie] = []
    var fetchingMore = false
    /// Enables/Disables cycling swipes on the tabBar controller. default value is 'false'
    open var isCyclingEnabled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "NavigationView", bundle: Bundle.main)
        let topRatedVC = storyboard.instantiateViewController(withIdentifier: "NavigationTMDBView")
        
        let searchVC = storyboard.instantiateViewController(withIdentifier: "NavigationViewC")

        topRatedVC.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 0)
        searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
        
        let tabBarList = [topRatedVC, searchVC]
        
        viewControllers = tabBarList
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

}
