//
//  MovieRepository.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//


import Foundation
import SwiftyJSON

struct MovieRepository: RestAPI {
    
    func getAll(page:Int = 0, completion:@escaping (RestResponse<[JSON]>) -> Void) {
        
        let url = "\(Constants.Network.baseURL)movie/top_rated?api_key=\(Constants.Session.apiKey)&page=\(page)"
        
        callAFService(withURl: url, httpMethod: .get) { (response) in
            switch response {
            case .success(let json):
                if let message = json["message"].string {
                    let error = NSError(domain: "AM",
                                        code: -1999,
                                        userInfo: [NSLocalizedDescriptionKey:message])
                    completion(.failure(error))
                    return
                }
                let results = json["results"].arrayValue
                completion(.success(results))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
