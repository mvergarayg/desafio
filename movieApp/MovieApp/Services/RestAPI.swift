//
//  RestAPI.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import Alamofire
import SwiftyJSON
import KeychainSwift

// Enum de valores asociados
enum RestResponse<T> {
    
    case success(T)
    case failure(Error)
    
}

protocol RestAPI {
    
    func callAFService(withURl urlString:String,
                       httpMethod method:HTTPMethod,
                       headers:[String:String]?,
                       payload:[String:Any]?,
                       completion:@escaping (RestResponse<JSON>) -> Void)
    
}

extension RestAPI {
    
    func callAFService(withURl urlString:String,
                       httpMethod method:HTTPMethod,
                       headers:[String:String]? = nil,
                       payload:[String:Any]? = nil,
                       completion:@escaping (RestResponse<JSON>) -> Void) {
        
        
        let request = Alamofire.request(urlString,
                                        method: method,
                                        parameters: payload,
                                        encoding: JSONEncoding.default)
        request.validate().responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(.success(json))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func callService(withURl urlString:String,
                     httpMethod method:String,
                     headers:[String:String]? = nil,
                     payload:[String:Any]? = nil) {
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.allHTTPHeaderFields = headers
        
        if let body = payload {
            let dataBody = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            request.httpBody = dataBody
        }
        
        let configuracion = URLSessionConfiguration.default
        let session = URLSession(configuration: configuracion)
        let tarea = session.dataTask(with: request) { (data, response, error) in
            
            OperationQueue.main.addOperation {
                    
                // Error solo valida si es que el server no retorna respuesta
                if let anError = error {
                    print("Ocurrio un error \(anError)")
                    return
                }
                
                // Validamos el response code
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode >= 200,
                    httpResponse.statusCode <= 299,
                    let myData = data else {
                        print("Error respuesta incorrecta")
                        return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: myData, options: .allowFragments)
                }
                catch(let ex) {
                    print(ex)
                }
            }
        }
        tarea.resume() // Ejecuta/invocación el web service
    }
    
}
