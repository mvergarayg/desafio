//
//  FavoriteTableViewController+Updating.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit

extension FavoriteTableViewController: UISearchResultsUpdating {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        let count: Int
        let resultsController = searchController.searchResultsController as? ResultsTableController
            
        if !searchController.searchBar.text!.isEmpty && (offsetY+scrollView.frame.height) > contentHeight && !fetchingMore && (resultsController?.filteredMovies.count)! > 0 {
            let whitespaceCharacterSet = CharacterSet.whitespaces
            let strippedString =
            searchController.searchBar.text!.trimmingCharacters(in: whitespaceCharacterSet)
            
            count = (((resultsController?.filteredMovies.count)! + 1) / 20) + 1
        
            getMovies(query: strippedString, page: count) { (response) in
                 switch response {
                   case .failure(let error):
                       self.showAlert(withTitle: "Error", message: error.localizedDescription)
                       
                   case .success(let movies):
                    for movie in movies {
                        resultsController?.filteredMovies.append(movie)
                    }
                    
                    self.fetchingMore = false
                    resultsController?.tableView.reloadData()
                   }
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        // Update the filtered array based on the search text.
        let whitespaceCharacterSet = CharacterSet.whitespaces
        let strippedString =
        searchController.searchBar.text!.trimmingCharacters(in: whitespaceCharacterSet)
        let count: Int
        
        let resultsController = searchController.searchResultsController as? ResultsTableController
            
        if resultsController?.filteredMovies.count == 0 {
            count = 1;
        } else {
            count = (((resultsController?.filteredMovies.count ?? 0) + 1) / 20) + 1
        }
        
        if !searchController.searchBar.text!.isEmpty {
            getMovies(query: strippedString, page: count) { (response) in
                 switch response {
                   case .failure(let error):
                       self.showAlert(withTitle: "Error", message: error.localizedDescription)
                       
                   case .success(let movies):
                       resultsController?.filteredMovies = movies
                       resultsController?.tableView.reloadData()
                }
            }
        }
    }
}


