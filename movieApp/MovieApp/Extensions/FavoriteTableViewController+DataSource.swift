//
//  FavoriteTableViewController+DataSource.swift
//  MovieApp
//
//  Created by Vergaray, Marco on 9/03/20.
//  Copyright © 2020 Marco Vergaray. All rights reserved.
//

import UIKit
import TraktKit

extension FavoriteTableViewController {

    func movie(forIndexPath: IndexPath) -> Movie {
        return movies[forIndexPath.row]
    }
    
    func getMovies(query: String, page: Int, completion: @escaping (RestResponse<[Movie]>) -> Void) {
        fetchingMore = true
        
        print("Getting movies from Trakt. Page: \(page)")
        
        TraktManager.sharedManager.search(query: query, types: [.movie],
                                          extended: [], pagination: .init(page: page, limit: 20), filters: nil)
        { [weak self] result in
            switch result {
                case .success(let results):
                DispatchQueue.main.async { [weak self] in
                    var resultsM = [Movie]()
                    for item in results {
                        let iMovie = Movie()
                        iMovie.title = item.movie?.title ?? ""
                        iMovie.id = "\(String(describing: item.movie?.ids.trakt))"
                        iMovie.overview = item.movie?.overview ?? ""
                        
                        iMovie.release_date = "\(item.movie?.year)"
                        iMovie.vote_count = "0"
                        resultsM.append(iMovie)
                    }
                    
                    self?.fetchingMore = false
                    
                    completion(.success(resultsM))
                }
                case .error(let error):
                    self?.fetchingMore = false
                    print("Failed to get user profile: \(String(describing: error?.localizedDescription))")
            }
        
        }
    }
}
